# LeadStation

#### Recommended version to work

##### This project work with node v10.19.0 that is a LTS version. It's already configured to work with it, if you use nvm you can type:

```
nvm use
```

#### Install

Install dependencies

```
yarn or yarn install
```

#### Build a bundle

```
yarn build
```

#### Run tests

```
yarn test
```

#### Run development server

```
yarn start
```
