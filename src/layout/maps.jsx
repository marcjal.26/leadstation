import React, { Component } from "react";
import PersonalMaps from "../components/PersonalMaps";

class MapsPage extends Component {
  render() {
    return (
      <div className="map">
        <PersonalMaps />
      </div>
    );
  }
}

export default MapsPage;
