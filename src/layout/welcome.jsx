import React, { Component } from "react";
import Typing from "react-typing-animation";

class WelcomePage extends Component {
  render() {
    return (
      <div className="welcome">
        <div className="welcome__header">
          <h1 className="welcome__header__headline">
            <Typing speed={200}>LeadStation</Typing>
          </h1>
          <hr className="divider" />
        </div>
        <div className="welcome__footer">
          <a className="welcome__footer__btn" href="/#/search">
            Search
          </a>
        </div>
      </div>
    );
  }
}

export default WelcomePage;
