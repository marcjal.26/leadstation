import React, { Component } from "react";
import GoogleApiWrapper from "../components/MapContainer";

const MY_API_KEY = `${process.env.REACT_APP_API_KEY}`;

class SearchPage extends Component {
  render() {
    return <GoogleApiWrapper apiKey={MY_API_KEY} />;
  }
}

export default SearchPage;
