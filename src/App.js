import React from "react";

import WelcomePage from "./layout/welcome";
import SearchPage from "./layout/search";
import MapsPage from "./layout/maps";

import "./App.scss";
import { HashRouter, Route, Switch, NavLink } from "react-router-dom";

const Main = () => {
  const links = [
    { name: "welcome", url: "/" },
    { name: "search", url: "/search" },
    { name: "my maps", url: "/maps" },
  ];
  return (
    <>
      <nav className="navbar">
        {links.map((item) => (
          <span key={item.url} className="navbar__item">
            <NavLink to={item.url}>{item.name}</NavLink>
          </span>
        ))}
      </nav>
    </>
  );
};

const App = () => (
  <HashRouter>
    <>
      <Main />
      <Switch>
        <Route exact path="/" component={WelcomePage} />
        <Route exact path="/search" component={SearchPage} />
        <Route exact path="/maps" component={MapsPage} />
        <Route path="*" component={Main} />
      </Switch>
    </>
  </HashRouter>
);

export default App;
