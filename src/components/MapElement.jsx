import React, { Component } from "react";

class MapElement extends Component {
  render() {
    return (
      <>
        <p className="map__place__city">{this.props.name}</p>
        <p className="map__place__coordinates">
          lat: {Math.round(this.props.lat * 100) / 100} lng:{" "}
          {Math.round(this.props.lng * 100) / 100}
        </p>
      </>
    );
  }
}

export default MapElement;
