import React, { Component } from "react";
import MapElement from "./MapElement";
import placesApiObject from "../services/PlacesApi";
import Typing from "react-typing-animation";

import { NavLink } from "react-router-dom";

class PersonalMaps extends Component {
  constructor(props) {
    super(props);

    this.state = {
      height: 0,
      mapElements: [],
      selected: {},
      displayPictures: "none",
    };

    this.loadAllElements();
  }

  loadAllElements() {
    let places = placesApiObject.getPlaces();
    places.map((item) => this.addMapElement(item));
  }

  loadImagesForSelected() {
    if (this.state.selected.imageUrls === undefined) {
      return [];
    }

    return this.state.selected.imageUrls.map((item, index) => (
      <li key={index} className="map__gallery__photos__photo">
        <img src={item} alt="" />
      </li>
    ));
  }

  addMapElement(item) {
    if (this.searchItemByIndex(item.id) !== undefined) {
      return;
    }

    let newMapElements = this.state.mapElements.push(
      <div
        key={item.id}
        className="map__place"
        onClick={() => this.selectItem(item.id)}
      >
        <MapElement
          key={item.id}
          id={item.id}
          name={item.name}
          lat={item.lat}
          lng={item.lng}
          imageUrls={item.imageUrls}
        />
      </div>
    );

    this.setState({
      mapElements: newMapElements,
    });
  }

  removeMapElement(e, id) {
    e.preventDefault();
    let newMapElements = this.state.mapElements.filter(
      (mapElement) => mapElement.props.children.props.id !== id
    );

    this.setState({
      mapElements: [...newMapElements],
      selected: {},
    });

    placesApiObject.deletePlace(id);

    this.toggle();
  }

  toggle = () => {
    this.setState((prevState) => ({
      height: prevState.height === 0 ? "auto" : 0,
    }));
  };

  searchItemByIndex(id) {
    for (var i = 0; i < this.state.mapElements.length; i++) {
      if (this.state.mapElements[i].props.children.props.id === id) {
        return this.state.mapElements[i].props.children.props;
      }
    }
  }

  selectItem = (id) => {
    this.toggle();
    let selectedItem = this.searchItemByIndex(id);

    this.setState({
      selected: {
        id: selectedItem.id,
        lat: selectedItem.lat,
        lng: selectedItem.lng,
        imageUrls: selectedItem.imageUrls,
      },
    });
  };
  showPhotos = (e) => {
    e.preventDefault();
    this.setState({
      displayPictures: "block",
    });
  };

  closePhotos = (e) => {
    e.preventDefault();
    this.setState({
      displayPictures: "none",
    });
    this.toggle();
  };

  arr = [];

  componentWillMount() {
    this.arr = placesApiObject.getPlaces();
  }

  renderMapElements() {
    if (this.state.mapElements.length > 0) {
      return <>{[...this.state.mapElements]}</>;
    } else {
      return (
        <h1 className="map__message">
          <Typing speed={200}>No maps saved</Typing>
        </h1>
      );
    }
  }

  render() {
    return (
      <>
        {this.renderMapElements()}

        <div
          className="map__gallery"
          style={{ display: this.state.displayPictures }}
        >
          <ul className="map__gallery__photos">
            {[...this.loadImagesForSelected()]}
          </ul>
        </div>

        <div className="map__footer">
          <NavLink
            to={`/search?lat=${this.state.selected.lat}&lng=${this.state.selected.lng}`}
            className="map__footer__btn"
          >
            Show on the map
          </NavLink>
          <a
            target="_blank"
            rel="noopener noreferrer"
            style={{ paddingTop: 8 }}
            href={`https://maps.google.com/?q=${this.state.selected.lat},${this.state.selected.lng}`}
            className="map__footer__btn"
          >
            Link to Google
          </a>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="!#"
            onClick={this.showPhotos}
            className="map__footer__btn"
          >
            See pictures
          </a>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="!#"
            className="map__footer__btn"
            onClick={this.closePhotos}
          >
            Remove pictures
          </a>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="!#"
            className="map__footer__btn"
            onClick={(e) => this.removeMapElement(e, this.state.selected.id)}
          >
            Delete
          </a>
        </div>
      </>
    );
  }
}

export default PersonalMaps;
