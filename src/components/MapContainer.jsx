import React, { Component } from "react";
import Map, { GoogleApiWrapper, InfoWindow, Marker } from "google-maps-react";
import Geosuggest from "react-geosuggest";
import placesApiObject from "../services/PlacesApi";

const mapStyles = {
  width: "100%",
  height: "100%",
};

export class MapContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
      position: {
        lat: -30.1087957,
        lng: -51.3172257,
        name: "Porto Alegre",
        id: "0",
      },
      markerLat: "",
      markerLng: "",
      mapZoom: 6,
    };

    this.urlParams = {};
    this.getUrlParams();

    if (this.urlParams.lat !== undefined && this.urlParams.lng !== undefined) {
      this.state.position.lat = this.urlParams.lat;
      this.state.position.lng = this.urlParams.lng;
      this.state.mapZoom = 7;
    }
  }

  map = {};

  getUrlParams() {
    if (document.location.hash.indexOf("?") === -1) {
      return;
    }

    let paramStrings = document.location.hash.split("?")[1].split("&");
    for (let i in paramStrings) {
      let split = paramStrings[i].split("=");
      this.urlParams[split[0]] = split[1];
    }
  }

  onMarkerClick = (props, marker, e) =>
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true,
    });

  onMapClicked = () => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null,
      });
    }
  };

  onClose = () => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: true,
        activeMarker: null,
      });
    }
  };

  onChange = (e) => {
    if (e.target.name === "lat") this.setState({ markerLat: e.target.value });
    if (e.target.name === "lng") this.setState({ markerLng: e.target.value });
  };

  fetchPlaces(mapProps, map) {
    const { google } = mapProps;
    new google.maps.places.PlacesService(map);
  }

  addMarker(lat, lng) {
    this.setState({
      position: {
        lat: lat,
        lng: lng,
      },
    });
  }

  suggestSelect(suggest) {
    if (suggest !== undefined) {
      let photoAray = [];

      if (suggest.gmaps.photos !== undefined) {
        photoAray = suggest.gmaps.photos.map((photo) => photo.getUrl());
      }

      this.setState({
        position: {
          lat: suggest.location.lat,
          lng: suggest.location.lng,
          name: suggest.label,
          id: suggest.placeId,
          imageUrls: photoAray,
        },
      });

      this.setState({
        mapZoom: 7,
      });

      this._map.setState({
        currentLocation: {
          lat: suggest.location.lat,
          lng: suggest.location.lng,
        },
      });
    }
  }

  addPlace() {
    placesApiObject.addPlace(
      this.state.position.id,
      this.state.position.lat,
      this.state.position.lng,
      this.state.position.name,
      this.state.position.imageUrls
    );

    this._geoSuggest.blur();
    this._geoSuggest.focus();
    this._geoSuggest.clear();
  }

  render() {
    return (
      <>
        <div className="search__box">
          <div className="search__box__coordinates">
            <input
              className="search__box__coordinates__input"
              type="text"
              onChange={this.onChange.bind(this)}
              placeholder="Type your latitude"
              name="lat"
              value={this.state.markerLat}
            />
            <input
              className="search__box__coordinates__input"
              type="text"
              onChange={this.onChange.bind(this)}
              placeholder="Type your longitude"
              name="lng"
              value={this.state.markerLng}
            />
            <button
              onClick={() =>
                this.addMarker(this.state.markerLat, this.state.markerLng)
              }
              className="search-btn"
            >
              Save
            </button>
          </div>
          <div className="search__box__places">
            <form className="geosuggest" onSubmit={this.onSubmit}>
              <Geosuggest
                ref={(el) => (this._geoSuggest = el)}
                placeholder=" Search for places"
                onSuggestSelect={(suggest) => {
                  this.suggestSelect(suggest);
                }}
              />
            </form>
            <button
              className="search-btn"
              onClick={() => {
                this.addPlace();
              }}
            >
              Save
            </button>
          </div>
        </div>
        <Map
          ref={(el) => (this._map = el)}
          className="map"
          google={this.props.google}
          zoom={this.state.mapZoom}
          style={mapStyles}
          onReady={this.fetchPlaces}
          onClick={this.onMapClicked(this)}
          initialCenter={{
            lat: this.state.position.lat,
            lng: this.state.position.lng,
          }}
        >
          <Marker
            onClick={this.onMarkerClick}
            draggable={true}
            name={"Porto Alegre"}
            position={this.state.position}
          />
          <InfoWindow
            marker={this.state.activeMarker}
            visible={this.state.showingInfoWindow}
            onClose={this.onClose}
          >
            <div>
              <h2>{this.state.selectedPlace.name}</h2>
            </div>
          </InfoWindow>
        </Map>
      </>
    );
  }
}

const LoadingContainer = (props) => <div> </div>;
export default GoogleApiWrapper((props) => ({
  apiKey: `${process.env.REACT_APP_API_KEY}`,
  language: props.language,
  LoadingContainer: LoadingContainer,
}))(MapContainer);
